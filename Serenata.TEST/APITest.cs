using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serenata.CORE.UnitOfWork;
using Serenata.DATA.DbContext;
using Serenata.DATA.Models;
using System;
using System.Linq;
using Xunit;

namespace Serenata.TEST
{

    public class APITest
    {
        private static DbContextOptions<ApplicationDbContext> CreateNewContextOptions()
        {
            // Create a fresh service provider, and therefore a fresh 
            // InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseInMemoryDatabase(databaseName: "SerenataDB")
                   .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }
        private void Seed(ApplicationDbContext context)
        {
            context.Products.AddRange(new Product
            {
                Id = 1,
                PictureURL = "https://res.cloudinary.com/serenata-commerce-limited/image/upload/f_auto,q_auto/t_Product400s/v1/Raw/110120_overview",
                Description = "Spot a rainbow on a cloudy day. Give a hug when they expect a handshake. Send flowers when they only expect a thank you note. This joyful cocktail of Lilies, Stocks, Asters, Solidago and Roses will arrive effortlessly in their home through the letterbox.",
                Name = "Floral Ratatouille",
                CreatedDate = DateTime.Now,
                Qty = 99,
                Price = 30
            },
               new Product
               {
                   Id = 2,
                   PictureURL = "https://res.cloudinary.com/serenata-commerce-limited/image/upload/f_auto,q_auto/t_Product400s/t_overlay_bestseller_nb/v1/Raw/110167_overview",
                   Description = "As the world outside begins to change, capture the moment with this stylish bouquet of beautiful cerise and red roses, glorious orange lily, yellow Solidago and red Hypericum berries.",
                   Name = "Changing Seasons",
                   CreatedDate = DateTime.Now,
                   Qty = 90,
                   Price = 30
               },
               new Product
               {
                   Id = 3,
                   PictureURL = "https://res.cloudinary.com/serenata-commerce-limited/image/upload/f_auto,q_auto/t_Product400s/t_overlay_hurry_nb/v1/Raw/108519_overview",
                   Description = "Take a stroll through a mystic wonderland of brandy roses hiding away from fairy freesias and making their way through the forest of purple lisianthus.",
                   Name = "Freya",
                   CreatedDate = DateTime.Now,
                   Qty = 99,
                   Price = 40
               });
            context.SaveChanges();
            var hasher = new PasswordHasher<ApplicationUser>();
            context.Users.Add(new ApplicationUser
            {
                Id = "57a4fcb0-04ec-4eef-99b1-c96619a20a90",
                Email = "test@test.com",
                NormalizedEmail = "TEST@TEST.COM",
                UserName = "Test",
                NormalizedUserName = "TEST",
                PasswordHash = hasher.HashPassword(null, "SuperAdmin007!"),
                SecurityStamp = new Guid().ToString(),
                EmailConfirmed = true,
                CreatedDate = DateTime.Now
            });
        }
        #region ProductRepo

        [Fact]
        public void CheckGetProductsNumber()
        {
            using (var context = new ApplicationDbContext(CreateNewContextOptions()))
            {
                Seed(context);
                IUnitOfWork uow = new UnitOfWork(context);
                Assert.True(uow.Products.Get().Result.Count() == 3);

            }

        }
        #endregion
        #region UserProductRepo
        [Fact]
        public void CheckCreateNormalUserProduct()
        {
            using (var context = new ApplicationDbContext(CreateNewContextOptions()))
            {
                Seed(context);
                IUnitOfWork uow = new UnitOfWork(context);
                UserProduct up = new UserProduct();
                up.ProductId = 1;
                up.UserId = "57a4fcb0-04ec-4eef-99b1-c96619a20a90";
                uow.UserProducts.Post(up);
                uow.Complete();
                Assert.True(uow.UserProducts.Get().Result.Count() == 1);

            }
        }
        [Fact]
        public void CheckGetByIdUserProduct()
        {
            using (var context = new ApplicationDbContext(CreateNewContextOptions()))
            {
                Seed(context);
                IUnitOfWork uow = new UnitOfWork(context);
                UserProduct up = new UserProduct();
                up.ProductId = 1;
                up.UserId = "57a4fcb0-04ec-4eef-99b1-c96619a20a90";
                uow.UserProducts.Post(up);
                uow.Complete();

                Assert.NotNull(uow.UserProducts.GetOne(1).Result);

            }
        }
        [Fact]
        public async System.Threading.Tasks.Task CheckGetByWrongIdUserProductAsync()
        {
            using (var context = new ApplicationDbContext(CreateNewContextOptions()))
            {
                Seed(context);
                IUnitOfWork uow = new UnitOfWork(context);
                UserProduct up = new UserProduct();
                up.ProductId = 1;
                up.UserId = "57a4fcb0-04ec-4eef-99b1-c96619a20a90";
                await uow.UserProducts.Post(up);
                await uow.Complete();
                UserProduct foundedUP = await uow.UserProducts.GetOne(42);
                Assert.True(foundedUP == null);
            }
        }
        [Fact]
        public void CheckCreateMissingProductIdUserProduct()
        {
            using (var context = new ApplicationDbContext(CreateNewContextOptions()))
            {
                Seed(context);
                IUnitOfWork uow = new UnitOfWork(context);
                UserProduct up = new UserProduct();
                up.UserId = "57a4fcb0-04ec-4eef-99b1-c96619a20a90";
                Action act = () => uow.UserProducts.Post(up);
                Exception exception = Assert.Throws<Exception>(act);
                Assert.Equal("This ProductId doesn't exists", exception.Message);
                uow.Complete();
            }
        }
        [Fact]
        public void CheckCreateMissingUserIdUserProduct()
        {
            using (var context = new ApplicationDbContext(CreateNewContextOptions()))
            {
                Seed(context);
                IUnitOfWork uow = new UnitOfWork(context);
                UserProduct up = new UserProduct();
                up.ProductId = 1;
                Action act = () => uow.UserProducts.Post(up);
                Exception exception = Assert.Throws<Exception>(act);
                Assert.Equal("This UserId doesn't exists", exception.Message);
                uow.Complete();
            }
        }
        [Fact]
        public void CheckCreateAlreadyExistingProductUserProduct()
        {
            using (var context = new ApplicationDbContext(CreateNewContextOptions()))
            {
                Seed(context);
                IUnitOfWork uow = new UnitOfWork(context);
                UserProduct up = new UserProduct();
                up.ProductId = 1;
                up.UserId = "57a4fcb0-04ec-4eef-99b1-c96619a20a90";
                uow.UserProducts.Post(up);
                uow.Complete();
                Action act = () => uow.UserProducts.Post(up);
                Exception exception = Assert.Throws<Exception>(act);
                Assert.Equal("There is an already existing product associated with this basket", exception.Message);
                uow.Complete();
            }
        }
        [Fact]
        public void CheckUpdateUserProduct()
        {
            using (var context = new ApplicationDbContext(CreateNewContextOptions()))
            {
                Seed(context);
                IUnitOfWork uow = new UnitOfWork(context);
                UserProduct up = new UserProduct();
                up.ProductId = 1;
                up.UserId = "57a4fcb0-04ec-4eef-99b1-c96619a20a90";
                up.Qty = 12;
                uow.UserProducts.Post(up);
                uow.Complete();
                up.Qty = 42;
                uow.UserProducts.Put(1,up);
                uow.Complete();
                Assert.True(uow.UserProducts.GetOne(1).Result.Qty == 42);
            }
        }
        [Fact]
        public async System.Threading.Tasks.Task CheckDeleteUserProductAsync()
        {
            using (var context = new ApplicationDbContext(CreateNewContextOptions()))
            {
                Seed(context);
                IUnitOfWork uow = new UnitOfWork(context);
                UserProduct up = new UserProduct();
                up.ProductId = 1;
                up.UserId = "57a4fcb0-04ec-4eef-99b1-c96619a20a90";
                up.Qty = 12;
                await uow.UserProducts.Post(up);
                await uow.Complete();
                await uow.UserProducts.Delete(1);
                await uow.Complete();
                UserProduct tmpUserProduct = await uow.UserProducts.GetOne(1);
                Assert.True(tmpUserProduct.DeletedDate > DateTime.MinValue);
            }
        }
        #endregion
    }
}
