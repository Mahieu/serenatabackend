﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Serenata.DATA.Models
{
    public class ApplicationUser : IdentityUser
    {
        [DataType(DataType.DateTime, ErrorMessage = "The DeletedDate should be a DateTime")]
        public DateTime DeletedDate { get; set; }
        [Required(ErrorMessage = "The CreatedDate is required")]
        [DataType(DataType.DateTime, ErrorMessage = "The CreatedDate should be a DateTime")]
        public DateTime CreatedDate { get; set; }
        [DataType(DataType.DateTime, ErrorMessage = "The UpdatedDate should be a DateTime")]
        public DateTime UpdatedDate { get; set; }
        [DataType(DataType.DateTime, ErrorMessage = "The BlockedDate should be a DateTime")]
        public DateTime BlockedDate { get; set; }
        //I could use a "Basket" entity, but it's not necessary due to the app complexity
        public virtual ICollection<UserProduct> UserProducts { get; set; }
    }
}
