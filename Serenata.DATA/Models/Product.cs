﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Serenata.DATA.Models
{
    public class Product : BaseInformation
    {
        [DataType(DataType.Text, ErrorMessage = "The Description should be text")]
        [StringLength(256, ErrorMessage = "The max length is 2048 character")]
        public string Name { get; set; }
        [DataType(DataType.Text, ErrorMessage = "The Description should be text")]
        [StringLength(2048, ErrorMessage = "The max length is 2048 character")]
        public string Description { get; set; }
        [DataType(DataType.ImageUrl, ErrorMessage = "The ImgUrl should be an url pointing to an image")]

        [StringLength(2048, ErrorMessage = "The max length is 2048 character")]
        public string PictureURL { get; set; }
        [Required(ErrorMessage = "The Qty is required")]
        [Range(0, int.MaxValue)]
        public int Qty { get; set; }
        [Required(ErrorMessage = "The Price is required")]
        [Range(0, float.MaxValue)]
        public float Price { get; set; }
        public virtual ICollection<UserProduct> UserProducts { get; set; }
    }
}
