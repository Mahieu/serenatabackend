﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Serenata.DATA.Models
{
    public class UserProduct : BaseInformation
    {
        [Required(ErrorMessage = "The UserId is required")]
        public string UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        [Required(ErrorMessage = "The ProductId is required")]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        [Required(ErrorMessage = "The Qty is required")]
        [Range(1, int.MaxValue)]
        public int Qty { get; set; }
    }
}
