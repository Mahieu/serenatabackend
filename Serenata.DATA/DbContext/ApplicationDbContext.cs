﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Serenata.DATA.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Serenata.DATA.DbContext
{
    public class ApplicationDbContext: IdentityDbContext<ApplicationUser>
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<UserProduct> UserProducts { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            /*
             I could use a seed class that is called when the project start, but I did it this ways because people are more used to.
             */
            base.OnModelCreating(builder);
            builder.Entity<UserProduct>()
    .HasKey(up => new {up.Id});
            builder.Entity<UserProduct>()
                .HasOne(up => up.ApplicationUser)
                .WithMany(p => p.UserProducts)
                .HasForeignKey(up => up.UserId);
            builder.Entity<UserProduct>()
                .HasOne(up => up.Product)
                .WithMany(p => p.UserProducts)
                .HasForeignKey(up => up.ProductId);
            var hasher = new PasswordHasher<ApplicationUser>();
            builder.Entity<ApplicationUser>().HasData(
                new ApplicationUser
                {
                    Id = "57a4fcb0-04ec-4eef-99b1-c96619a20a90",
                    Email = "test@test.com",
                    NormalizedEmail = "TEST@TEST.COM",
                    UserName = "Test",
                    NormalizedUserName = "TEST",
                    PasswordHash = hasher.HashPassword(null, "SuperAdmin007!"),
                    SecurityStamp = new Guid().ToString(),
                    EmailConfirmed = true,
                    CreatedDate = DateTime.Now
                });
            builder.Entity<Product>().HasData(
               new Product
               {
                   Id = 1,
                   PictureURL = "https://res.cloudinary.com/serenata-commerce-limited/image/upload/f_auto,q_auto/t_Product400s/v1/Raw/110120_overview",
                   Description = "Spot a rainbow on a cloudy day. Give a hug when they expect a handshake. Send flowers when they only expect a thank you note. This joyful cocktail of Lilies, Stocks, Asters, Solidago and Roses will arrive effortlessly in their home through the letterbox.",
                   Name = "Floral Ratatouille",
                   CreatedDate = DateTime.Now,
                   Qty = 99,
                   Price = 30
               },
               new Product
               {
                   Id = 2,
                   PictureURL = "https://res.cloudinary.com/serenata-commerce-limited/image/upload/f_auto,q_auto/t_Product400s/t_overlay_bestseller_nb/v1/Raw/110167_overview",
                   Description = "As the world outside begins to change, capture the moment with this stylish bouquet of beautiful cerise and red roses, glorious orange lily, yellow Solidago and red Hypericum berries.",
                   Name = "Changing Seasons",
                   CreatedDate = DateTime.Now,
                   Qty = 90,
                   Price = 30
               },
               new Product
               {
                   Id = 3,
                   PictureURL = "https://res.cloudinary.com/serenata-commerce-limited/image/upload/f_auto,q_auto/t_Product400s/t_overlay_hurry_nb/v1/Raw/108519_overview",
                   Description = "Take a stroll through a mystic wonderland of brandy roses hiding away from fairy freesias and making their way through the forest of purple lisianthus.",
                   Name = "Freya",
                   CreatedDate = DateTime.Now,
                   Qty = 99,
                   Price = 40
               }
               );
        }
        /*
            This allows the soft-delete capability without any change inside the repositories.
         */
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            ChangeTracker.DetectChanges();
            var markedAsDeleted = ChangeTracker.Entries().Where(x => x.State == EntityState.Deleted);
            var markedAsModified = ChangeTracker.Entries().Where(x => x.State == EntityState.Modified);
            var markedAsAdded = ChangeTracker.Entries().Where(x => x.State == EntityState.Added);
            foreach (var item in markedAsDeleted)
            {
                if (item.Entity is BaseInformation entity)
                {
                    // Set the entity to unchanged (if we mark the whole entity as Modified, every field gets sent to Db as an update)
                    item.State = EntityState.Unchanged;
                    // Only update the IsDeleted flag - only this will get sent to the Db
                    entity.DeletedDate = DateTime.Now;
                }
            }
            foreach (var item in markedAsModified)
            {
                if (item.Entity is BaseInformation entity)
                {
                    entity.UpdatedDate = DateTime.Now;
                }
            }
            foreach (var item in markedAsAdded)
            {
                if (item.Entity is BaseInformation entity)
                {
                    entity.CreatedDate = DateTime.Now;
                }
            }
            return (await base.SaveChangesAsync(true, cancellationToken));
        }

    }
}
