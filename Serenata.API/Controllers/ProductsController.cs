﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serenata.API.Models;
using Serenata.CORE.UnitOfWork;
using Serenata.DATA.Models;

namespace Serenata.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ProductsController(IUnitOfWork unitOfWork, IMapper mapper) {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> Get()
        {
            IEnumerable<Product> products = await _unitOfWork.Products.Get();
            IEnumerable<ProductAPI> res = _mapper.Map<IEnumerable<Product>, IEnumerable<ProductAPI>>(products);
            return Ok(res);
        }
        [HttpGet("{id}")]

        public async Task<ActionResult<Product>> Get(int id)
        {
            try
            {
                Product product = await _unitOfWork.Products.GetOne(id);
                ProductAPI res = _mapper.Map<Product, ProductAPI>(product);
                return Ok(res);
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
            
        }
    }
}
