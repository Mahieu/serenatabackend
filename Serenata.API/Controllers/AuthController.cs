﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Serenata.API.Models;
using Serenata.DATA.Models;

namespace Serenata.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _config;
        public AuthController(UserManager<ApplicationUser> userManager, IConfiguration config)
        {
            _userManager = userManager;
            _config = config;
        }
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginAPI loginAPI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ApplicationUser tmpUser = await _userManager.FindByEmailAsync(loginAPI.Email);
            bool isPasswordValid = await _userManager.CheckPasswordAsync(tmpUser, loginAPI.Password);
            if (tmpUser != null && isPasswordValid)
            {
                var roles = await _userManager.GetRolesAsync(tmpUser);
                List<Claim> authClaims = new List<Claim>();
                authClaims.Add(new Claim(JwtRegisteredClaimNames.Sub, tmpUser.Id));
                authClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
                foreach (var role in roles)
                {
                    authClaims.Add(new Claim("role", role));
                }
                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSetting:Token").Value));

                var token = new JwtSecurityToken(
                    expires: DateTime.Now.AddHours(23),
                    claims: authClaims,
                    signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );
                LoginResultAPI loginResultAPI = new LoginResultAPI();
                loginResultAPI.Token = new JwtSecurityTokenHandler().WriteToken(token);
                loginResultAPI.Roles = roles;
                loginResultAPI.Username = tmpUser.UserName;
                loginResultAPI.UserId = tmpUser.Id;
                loginResultAPI.ExpirationDate = token.ValidTo;
                return Ok(loginResultAPI);
            }
            return Unauthorized();
        }
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterAPI model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ApplicationUser tmpUser = await _userManager.FindByEmailAsync(model.Email);
            if (tmpUser == null)
            {
                ApplicationUser createdUser = new ApplicationUser
                {
                    Email = model.Email,
                    UserName = model.Username,
                    EmailConfirmed = true
                };
                IdentityResult res = _userManager.CreateAsync(createdUser, model.Password).Result;
                if (res.Succeeded)
                {
                    return Created(createdUser.Id, new RegisterResultAPI
                    {
                        Id = createdUser.Id,
                        Email = createdUser.Email
                    });
                }
                else
                {
                    return StatusCode(500);
                }
            }
            else
            {
                ModelState.AddModelError("Already existing user", "A user with this email/username already exists.");
                return Conflict(ModelState);
            }
        }
    }
}
