﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Serenata.API.Models;
using Serenata.CORE.UnitOfWork;
using Serenata.DATA.Models;

namespace Serenata.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserProductsController : ControllerBase
    {
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private string _currentUserId;
        private UserManager<ApplicationUser> _userManager;
        public UserProductsController(IUnitOfWork unitOfWork, IMapper mapper, IHttpContextAccessor httpContextAccessor, UserManager<ApplicationUser> userManager)
        {
            this._unitOfWork = unitOfWork;
            this._userManager = userManager;
            this._currentUserId = this._userManager.GetUserId(httpContextAccessor.HttpContext.User);
            this._mapper = mapper;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserProductAPI>>> Get()
        {
            /*
            For the basket, I could load it with the login of the user.
            But I preferer loading the basket by calling this action, and in the future, if there is an admin role.
            I would modify this action to allows the admin to load all User Products.
            */
            IEnumerable<UserProduct> userProducts = await _unitOfWork.UserProducts.Get(_currentUserId,true,false,0);
            IEnumerable<UserProductAPI> userProductsApi = _mapper.Map<IEnumerable<UserProduct>, IEnumerable<UserProductAPI>>(userProducts);
            return Ok(userProductsApi);
        }
        /*
         Normally in a perfect REST Api, we doesn't send back the created entity, but for me, it's better like this.
         Like that, the frontend receives the Id back.
         */
        [HttpPost]
        public async Task<ActionResult<UserProductAPI>> Post([FromBody] UserProductAPI userProductAPI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            /*
             If I wanted, I could just do this:  userProductAPI.UserId = _currentUserId
             But like that, if there is in the future an "Admin" role, we can modify the condition to allow him the ability to add products to someone basket.
             */
            if (userProductAPI.UserId != _currentUserId)
            {
                ModelState.AddModelError("Wrong UserId", "You are not allowed to add products to someone else basket.");
                return StatusCode(405, ModelState);
            }
            try
            {
                UserProduct userProduct = _mapper.Map<UserProductAPI, UserProduct>(userProductAPI);
                userProduct.ApplicationUser = null;
                userProduct.Product = null;
                UserProduct res = await _unitOfWork.UserProducts.Post(userProduct);
                await _unitOfWork.Complete();
                UserProductAPI resAPI = _mapper.Map<UserProduct, UserProductAPI>(res);
                return Ok(resAPI);
            }
            catch (Exception err)
            { 
                return BadRequest(err.Message);
            }
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<UserProductAPI>> Put(int id, [FromBody] UserProductAPI userProductAPI)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != userProductAPI.Id)
            {
                ModelState.AddModelError("Mismatching Id's", "The Id in the URL is different than in the model");
                return BadRequest(ModelState);
            }
            if (userProductAPI.UserId != _currentUserId)
            {
                ModelState.AddModelError("Wrong UserId", "You are not allowed to edit products to someone else basket.");
                return StatusCode(405, ModelState);
            }
            try
            {
                UserProduct tmpUP = _mapper.Map<UserProductAPI, UserProduct>(userProductAPI);
                await _unitOfWork.UserProducts.Put(id,tmpUP);
                await _unitOfWork.Complete();
                return StatusCode(201);
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _unitOfWork.UserProducts.Delete(id);
                await _unitOfWork.Complete();
                return Ok();
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
        }
    }
}
