﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Serenata.API.Models
{
    public class UserProductAPI
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="The UserId is required")]
        public string UserId { get; set; }
        public UserAPI User { get; set; }
        [Required(ErrorMessage = "The ProductId is required")]
        public int ProductId { get; set; }
        public ProductAPI Product { get; set; }
        [Required(ErrorMessage = "The Qty is required")]
        [Range(1, int.MaxValue)]
        public int Qty { get; set; }
    }
}
