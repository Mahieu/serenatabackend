﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Serenata.API.Models
{
    public class UserAPI
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        [DataType(DataType.EmailAddress, ErrorMessage = "The Email is not correct")]
        public string Email { get; set; }
        [DataType(DataType.Password, ErrorMessage = "The Password is not valid")]
        public string Password { get; set; }
        public ICollection<UserProductAPI> UserProducts { get; set; }

    }
}
