﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Serenata.API.Models
{
    public class LoginResultAPI
    {
        public string Username { get; set; }
        public string UserId { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public string Token { get; set; }
        public DateTime ExpirationDate { get; set; }

        public ICollection<UserProductAPI> UserProducts { get; set; }
    }
}
