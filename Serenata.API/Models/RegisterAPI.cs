﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Serenata.API.Models
{
    public class RegisterAPI
    {
        [Required(ErrorMessage = "The Username is required")]
        [DataType(DataType.Text, ErrorMessage = "The Username sould be text")]
        public string Username { get; set; }

        [Required(ErrorMessage = "The Password is required")]
        public string Password { get; set; }
        [Required(ErrorMessage = "The Email is required")]
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address.")]
        public string Email { get; set; }
    }
}
