﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Serenata.API.Models
{
    public class ErrorAPI
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
