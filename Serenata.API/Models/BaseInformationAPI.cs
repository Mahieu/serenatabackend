﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Serenata.API.Models
{
    public class BaseInformationAPI
    {
        public int Id { get; set; }
        [DataType(DataType.DateTime, ErrorMessage = "The DeletedDate should be a DateTime")]
        public DateTime DeletedDate { get; set; }
        [Required(ErrorMessage = "The CreatedDate is required")]
        [DataType(DataType.DateTime, ErrorMessage = "The CreatedDate should be a DateTime")]
        public DateTime CreatedDate { get; set; }
        [DataType(DataType.DateTime, ErrorMessage = "The UpdatedDate should be a DateTime")]
        public DateTime UpdatedDate { get; set; }
    }
}
