﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Serenata.API.Models
{
    public class RegisterResultAPI
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}
