﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serenata.API.Models;
using Serenata.DATA.Models;

namespace Serenata.API.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domain to Resource
            CreateMap<BaseInformation, BaseInformationAPI>();
            CreateMap<Product, ProductAPI>();
            CreateMap<UserProduct, UserProductAPI>();
            CreateMap<ApplicationUser, UserAPI>();

            // Resource to Domain
            CreateMap<BaseInformationAPI, BaseInformation>();
            CreateMap<ProductAPI, Product>();
            CreateMap<UserProductAPI, UserProduct>();
            CreateMap<UserAPI, ApplicationUser>();
        }
    }
}
