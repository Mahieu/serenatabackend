﻿using Serenata.CORE.Repositories;
using Serenata.DATA.DbContext;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Serenata.CORE.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private IProductRepository products;
        private IUserProductRepository userProducts;
        public IProductRepository Products
        {
            get
            {
                if (this.products == null)
                {
                    this.products = new ProductRepository(_context);
                }
                return this.products;
            }
        }
        public IUserProductRepository UserProducts
        {
            get
            {
                if (this.userProducts == null)
                {
                    this.userProducts = new UserProductRepository(_context);
                }
                return this.userProducts;
            }
        }
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
