﻿using Serenata.CORE.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Serenata.CORE.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository Products { get;}
        IUserProductRepository UserProducts { get; }
        Task<int> Complete();
    }
}
