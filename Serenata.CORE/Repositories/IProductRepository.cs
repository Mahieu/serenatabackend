﻿using Serenata.DATA.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Serenata.CORE.Repositories
{
    public interface IProductRepository : IGenericRepository<int, Product>
    {

    }
}
