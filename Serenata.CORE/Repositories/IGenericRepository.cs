﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Serenata.CORE.Repositories
{
    public interface IGenericRepository<TKey, T> where T : class
    {
        Task<IEnumerable<T>> Get();
        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
        Task<T> GetOne(TKey id);
        Task<T> Post(T entity);
        Task Put(TKey id, T entity);
        Task Delete(TKey id);
    }
}
