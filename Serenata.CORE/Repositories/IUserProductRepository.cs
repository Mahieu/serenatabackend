﻿using Serenata.DATA.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Serenata.CORE.Repositories
{
    public interface IUserProductRepository : IGenericRepository<int, UserProduct>
    {
        Task<IEnumerable<UserProduct>> Get(string userId,bool lld, bool lsdd, int productId);
    }
}
