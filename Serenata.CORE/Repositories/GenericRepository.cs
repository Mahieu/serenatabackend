﻿using Microsoft.EntityFrameworkCore;
using Serenata.DATA.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Serenata.CORE.Repositories
{
    public class GenericRepository<TKey, T> : IGenericRepository<TKey, T> where T : class
    {
        private readonly DbSet<T> _entities;
        private readonly ApplicationDbContext _context;
        public GenericRepository(ApplicationDbContext context)
        {
            _context = context;
            _entities = _context.Set<T>();

        }
        /// <summary>
        /// Delete an entity based on the Id.
        /// </summary>
        /// <param name="id">Id of the entity</param>
        /// <exception cref="System.Exception">Throws an exception when the entity is not found</exception>
        public virtual async Task Delete(TKey id)
        {
            T tmpEntity = await _entities.FindAsync(id);
            if (tmpEntity == null)
            {
                throw new Exception("Entity not found");
            }
            try
            {
                _entities.Remove(tmpEntity);
            }
            catch (Exception err)
            {
                throw err;
            }

        }
        /// <summary>
        /// Get all the entities
        /// </summary>
        /// <returns>List of entity</returns>
        public virtual async Task<IEnumerable<T>> Get()
        {
            return await _entities.ToListAsync();
        }
        /// <summary>
        /// Get one entity based on his primary key
        /// </summary>
        /// <param name="id">The primary key</param>
        /// <returns>An entity</returns>
        public async virtual Task<T> GetOne(TKey id)
        {
            return await _entities.FindAsync(id);
        }
        /// <summary>
        /// Create one entity
        /// </summary>
        /// <param name="entity">The entity to create</param>
        /// <exception cref="System.Exception">Throws an exception if something went bad</exception>
        /// <returns>The created entity</returns>
        public virtual async Task<T> Post(T entity)
        {
            try
            {
                var state = await _entities.AddAsync(entity);
                return entity;
            }
            catch (Exception err)
            {
                Console.WriteLine(err);
                throw err;
            }

        }
        /// <summary>
        /// Update an entity
        /// </summary>
        /// <param name="id">The id of the entity</param>
        /// <param name="entity">The entity to update</param>
        /// <exception cref="System.Exception">If the entity doesn't exists or when something went bad</exception>
        /// 
        public virtual async Task Put(TKey id, T entity)
        {
            T tmpEntity = await _entities.FindAsync(id);
            if (tmpEntity == null)
            {
                throw new Exception("Entity not found");
            }
            _context.Entry(tmpEntity).State = EntityState.Detached;
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
            }
            catch (Exception err)
            {
                throw err;
            }
        }
        /// <summary>
        /// Find an entity on criterias
        /// </summary>
        /// <param name="expression">The expression to look for</param>
        /// <returns>An IQueryable</returns>
        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this._entities.Where(expression).AsNoTracking();
        }
    }
}
