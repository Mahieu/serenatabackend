﻿using Microsoft.EntityFrameworkCore;
using Serenata.DATA.DbContext;
using Serenata.DATA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Serenata.CORE.Repositories
{
    public class UserProductRepository : GenericRepository<int, UserProduct>, IUserProductRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<UserProduct> _entities;
        public UserProductRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
            _entities = _context.Set<UserProduct>();
        }
        /// <summary>
        /// Retreive all the UserProducts based on criterias.
        /// </summary>
        /// <param name="userId">The User Id of the UserProduct.</param>
        /// <param name="lsdd">Load soft deleted data.</param>
        /// <param name="productId">The product id of the UserProduct.</param>
        /// <returns>Returns a list of UserProduct.</returns>
        public async Task<IEnumerable<UserProduct>> Get(string userId,bool lld, bool lsdd, int productId)
        {
            var request = from up in _entities select up ;
            if (userId.Length > 0)
            {
                request = request.Where(up => up.UserId == userId);
            }
            if (!lsdd)
            {
                request = request.Where(up => up.DeletedDate == DateTime.MinValue);
            }
            if (lld)
            {
                request = request.Include(up => up.Product);
            }
            if (productId > 0)
            {
                request = request.Where(up => up.ProductId == productId);
            }
            return await request.ToListAsync<UserProduct>();
        }
        public override Task<UserProduct> Post(UserProduct entity)
        {

            if (!CheckIfUserExists(entity.UserId))
            {
                throw new Exception("This UserId doesn't exists");
            }
            IEnumerable<UserProduct> userProductsAlreadyExisting = this.Get(entity.UserId,false,false, entity.ProductId).Result;
            if (userProductsAlreadyExisting.Any()) {
                throw new Exception("There is an already existing product associated with this basket");
            }
            if (!CheckIfProductExists(entity.ProductId))
            {
                throw new Exception("This ProductId doesn't exists");
            }
            try {
                return base.Post(entity);
            }
            catch(Exception err)
            {
                throw err;
            }
        }
        public override Task Put(int id, UserProduct entity)
        {
            if (!CheckIfProductExists(entity.ProductId))
            {
                throw new Exception("This ProductId doesn't exists");
            }
            if (!CheckIfUserExists(entity.UserId))
            {
                throw new Exception("This UserId doesn't exists");
            }
            try
            {
                return base.Put(id, entity);
            }
            catch (Exception err)
            {
                throw err;
            }
        }
        public override async Task Delete(int id)
        {
            UserProduct tmpUP = await this.GetOne(id);
            if (tmpUP == null || tmpUP.CheckIsDeleted())
            {
                throw new Exception("Product not found");
            }
            try
            {
                _entities.Remove(tmpUP);
            }
            catch (Exception err)
            {
                throw err;
            }
        }
        private bool CheckIfProductExists(int productId)
        {
            return _context.Products.Find(productId) != null;
        }
        private bool CheckIfUserExists(string userId)
        {
            return _context.Users.Find(userId) != null;
        }
    }
}

