﻿using Microsoft.EntityFrameworkCore;
using Serenata.DATA.DbContext;
using Serenata.DATA.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Serenata.CORE.Repositories
{
    public class ProductRepository : GenericRepository<int,Product>, IProductRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<Product> _entities;
        public ProductRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
            _entities = _context.Set<Product>();
        }
    }
}
